<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class CompanyRequest extends FormRequest
{

    protected $router;

    public function __construct(Route $router)
    {
        $this->router = $router;
    }

    public function wantsJson()
    {
        return true;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique',
            'image' => 'jpeg,jpg,png|required',
            'website' => 'required'
        ];
    }
}
