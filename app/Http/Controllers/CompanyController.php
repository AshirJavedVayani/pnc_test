<?php

namespace App\Http\Controllers;

use App\Company;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
//use App\Http\Requests\CompanyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\UrlGenerator;
use File;
use DB;

class CompanyController extends Controller
{

    protected $url;
//    protected $CompanyRequest;

    //constructor
    public function __construct(UrlGenerator $url)
    {
        $this->middleware('auth');
        $this->url = $url;
//        $this->companyRequest = $companyRequest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $comp = Company::paginate(10);
        $start = intval($request->get('start'));

        return view('header') . view('sidebar') . view('company', compact('comp','start')) . view('settings') . view('footer');

    }

    public function create()
    {

        return view('header') . view('sidebar') . view('addCompany') . view('settings') . view('footer');

    }

    //store company
    public function store(Request $request)
    {
        try {

            $this->validate($request,[
                'name' => 'required',
                'email' => 'required|email',
                'website' => 'required'
            ]);

            $input = $request->all();

//            print_r($input);die;

            $url = $this->url->to('/uploads/company_logo');

            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/company_logo');
            $image->move($destinationPath, $name);
            $input['logo'] = $url.'/'.$name;

            $company = Company::create($input);

//            $data = array(
//                'name' => $input['name']
//            );
//            print_r($company);die;

            Mail::to('ashir@engitechservices.com')->send(new SendEmail());
//            Mail::send(['text' => 'mail'],['name' , 'ashir'], function($message){
//                $message->to('ashirjv0221@gmail.com', 'For Test')->subject('Test Email');
//                $message->from('ashirjv0221@gmail.com', 'Test');
//            });

//            return back()->with('success','Company Added Successfully');

            //PUT HERE AFTER YOU SAVE
            \Session::flash('flash_message','Company Added Successfully.');

            return redirect('company/');

        } catch (\Exception $e) {
            \Log::debug('SaveUser: ' . $e->getMessage() . ' File: ' . $e->getFile() . ' Line: ' . $e->getLine());
            DB::rollback();
            $request->session()->flash('error', 'Company could not be added!');
        }
    }

    public function edit($CompID){

        $company = Company::getCompaniesByID($CompID);

        return view('header') . view('sidebar') . view('updateCompany', compact('company')) . view('settings') . view('footer');
    }

    //update company
    public function update(Request $request)
    {

        try {

            $url = $this->url->to('/uploads/company_logo');

            if ($request->file('image')){
                $input = $request->all();
                $CompID = $input['compID'];
                $company = Company::getCompaniesByID($CompID);

                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/company_logo');
                $image->move($destinationPath, $name);
                $input['logo'] = $url.'/'.$name;

                $company->name = $input['name'];
                $company->email = $input['email'];
                $company->logo = $input['logo'];
                $company->website = $input['website'];

                $company->save();
            }
            else{
                $input = $request->all();
                $CompID = $input['compID'];
                $company = Company::getCompaniesByID($CompID);

                $company->name = $input['name'];
                $company->email = $input['email'];
                $company->logo = $input['old_image'];
                $company->website = $input['website'];

                $company->save();
            }

            //PUT HERE AFTER YOU SAVE
            \Session::flash('flash_message','Company Updated Successfully.');

            return redirect('company/');

        } catch (\Exception $e) {
            \Log::debug('SaveUser: ' . $e->getMessage() . ' File: ' . $e->getFile() . ' Line: ' . $e->getLine());
            DB::rollback();
            $request->session()->flash('error', 'Company could not be updated!');
        }
    }

    //delete Company
    public function delete($CompID){

        $company = Company::getCompaniesByID($CompID);

        //delete image from folder
        $image_path = $company['logo'];
        $image_name = substr($image_path, strrpos($image_path, '/') + 1);
        $destinationPath = public_path('/uploads/company_logo/'.$image_name);

        if(File::exists($destinationPath)) {
            File::delete($destinationPath);
        }

        $company->delete();

        //PUT HERE AFTER YOU SAVE
        \Session::flash('flash_message','Company Deleted Successfully.');

        return redirect(url('company'))->with('success', 'Company Deleted Successfully!');
    }

}
