<?php

namespace App\Http\Controllers;

use App\User;
use App\Job;
use App\Employer;
use App\Category;
use App\Company;
use App\Cv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DashboardController extends Controller
{

	//constructor
	public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	return view('header') . view('sidebar') . view('dashboard') . view('settings') . view('footer');

    }

}
