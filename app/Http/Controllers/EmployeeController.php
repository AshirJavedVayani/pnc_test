<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Company;
use Illuminate\Http\Request;
//use App\Http\Requests\CompanyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\UrlGenerator;
use File;
use Input;
use DB;
use Log;

class EmployeeController extends Controller
{

    protected $url;

    //constructor
    public function __construct(UrlGenerator $url)
    {
        $this->middleware('auth');
        $this->url = $url;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $emp = Employee::getEmployees();
        $start = intval($request->get('start'));

        return view('header') . view('sidebar') . view('employee', compact('emp','start')) . view('settings') . view('footer');

    }

    public function create()
    {

        $company = Company::all('*');

        return view('header') . view('sidebar') . view('addEmployee', compact('company')) . view('settings') . view('footer');

    }

    //store employee
    public function store(Request $request)
    {
        try {

            $this->validate($request,[
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'phone' => 'required'
            ]);

            $input = $request->all();

            $employee = Employee::create($input);

            //PUT HERE AFTER YOU SAVE
            \Session::flash('flash_message','Employee Added Successfully.');

            return redirect('employee/');

        } catch (\Exception $e) {
            \Log::debug('SaveUser: ' . $e->getMessage() . ' File: ' . $e->getFile() . ' Line: ' . $e->getLine());
            DB::rollback();
            $request->session()->flash('error', 'Employee could not be added!');
        }
    }

    public function edit($EmpID){

        $employee = Employee::getEmployeesByID($EmpID);

        $company = Company::all('*');

        return view('header') . view('sidebar') . view('updateEmployee', compact('employee','company')) . view('settings') . view('footer');
    }

    //update employee
    public function update(Request $request)
    {

        try {

            $input = $request->all();
            $EmpID = $input['empID'];
            $employee = Employee::getEmployeesByID($EmpID);

            $employee->firstname = $input['firstname'];
            $employee->lastname = $input['lastname'];
            $employee->company_id = $input['company_id'];
            $employee->email = $input['email'];
            $employee->phone = $input['phone'];

            $employee->save();

            //PUT HERE AFTER YOU SAVE
            \Session::flash('flash_message','Employee Updated Successfully.');

            return redirect('employee/');

        } catch (\Exception $e) {
            \Log::debug('SaveUser: ' . $e->getMessage() . ' File: ' . $e->getFile() . ' Line: ' . $e->getLine());
            DB::rollback();
            $request->session()->flash('error', 'Employee could not be updated!');
        }
    }

    //delete Employee
    public function delete($EmpID){

        $employee = Employee::getEmployeesByID($EmpID);

        $employee->delete();

        //PUT HERE AFTER YOU SAVE
        \Session::flash('flash_message','Employee Deleted Successfully.');

        return redirect(url('employee'))->with('success', 'Employee Deleted Successfully!');
    }

}
