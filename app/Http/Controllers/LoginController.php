<?php

namespace App\Http\Controllers;

//use App\Category;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     use AuthenticatesUsers;

    public function index()
    {

//    	echo "ok";

    //if ( Auth::attempt(['MAIL_USERNAME' => $email, 'MAIL_PASSWORD' => $password]))
    //{
        //return redirect()->route('dboard');
    //}
		return view('login');

    }

    public function authenticate(Request $request){

		$this->validateLogin($request);
//		print_r($request);die;

		if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')] )){

    		return redirect('home');

		}
		else{
			return redirect()->back()->withErrors('Invalid Email/Password');
		}

	}

	public function logout(){

    	Auth::logout();
    	return redirect('/');
	}

}
