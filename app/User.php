<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

	protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name','father_name', 'user_type', 'image', 'email', 'password','cnic','contact_number','other_contact','qualification_id','experience','occupation_id','category_id','is_jobless','current_designation','company_id','current_salary','postal_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function company()
	{
		return $this->belongsTo('App\Company');
	}

	public function experience()
	{
		return $this->belongsTo('App\Experience');
	}

	public function occupation()
	{
		return $this->belongsTo('App\Occupation');
	}

	public function qualification()
	{
		return $this->belongsTo('App\Qualification');
	}

	public function cv()
	{
		return $this->hasMany('App\Cv');
	}

	//get employers
	public static function getEmployers(){

		return self::where('user_type', '2')->with(['category','company'])->orderBy('id', 'DESC')->get();
	}

	//get Employers By ID
	public static function getEmployersByID($EmpID){

		return self::where('id', $EmpID)->with(['category','company'])->first();
	}

	//get candidates
	public static function getCandidates(){

		return self::where('user_type', '1')->with(['category','company'])->orderBy('id', 'DESC')->get();
	}

	//get users
	public static function GetUserByID($userid){

		return self::where('id', $userid)->with(['category','company','experience','qualification','occupation'])->get();
	}

	//get Candidates By ID
	public static function getCandidatesByID($CandID){

		return self::where('id', $CandID)->with(['category','company'])->first();
	}

}
