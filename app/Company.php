<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Company extends Model
{

    protected $table = 'companies';

    protected $fillable = [
        'id',
        'name',
        'email',
        'logo',
        'website',
        'created_at',
        'updated_at'
    ];


    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function employees()
    {
        return $this->hasMany('App\Employee');
    }

    public static function ifCompanyExist($company_name){

        return self::where('title', $company_name)->first();
    }

    //get Company By ID
    public static function getCompaniesByID($CompID){

        return self::where('id', $CompID)->first();
    }
}
