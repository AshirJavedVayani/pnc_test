<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employee extends Model
{

    protected $table = 'employees';

    protected $fillable = [
        'id',
        'firstname',
        'lastname',
        'company_id',
        'email',
        'phone',
        'created_at',
        'updated_at'
    ];


    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    //get employees
    public static function getEmployees(){

        return self::with(['company'])->orderBy('id', 'DESC')->get();
    }

    //get Employee By ID
    public static function getEmployeesByID($EmpID){

        return self::where('id', $EmpID)->with(['company'])->first();
    }
}
