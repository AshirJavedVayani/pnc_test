<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Email extends Mailable
{
	use Queueable, SerializesModels;

	public $subject;
	public $body;

	public $tries = 3;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($subject, $body)
	{
		$this->subject = $subject;
		$this->body    = $body;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->markdown('emails.email')
			->subject($this->subject);
	}
}
