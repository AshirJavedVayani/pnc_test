<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update Company
            <small>Fill your company information below </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0)">Company</a></li>
            <li class="active">Update Company</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Company</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{url('updateComp')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Company Name" value="{{$company->name}}" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address" value="{{$company->email}}" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="image">Image</label>
                                <img src="{{$company->logo}}" id="target" width="200px;" height="150px;">
                                <input type="file" name="image" class="form-control" id="src">
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" name="website" class="form-control" id="website" placeholder="Enter Website" value="{{$company->website}}" required>
                            </div>

                        </div>
                        <input type="hidden" name="old_image" value="{{$company->logo}}">
                        <input type="hidden" name="compID" value="{{$company->id}}">
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" name="submit" id="sunbmit" class="btn btn-primary">Update Company</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <!--/.col (left) -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    function showImage(src,target) {
        var fr=new FileReader();
        // when image is loaded, set the src of the image where you want to display it
        fr.onload = function(e) { target.src = this.result; };
        src.addEventListener("change",function() {
            // fill fr with image data
            fr.readAsDataURL(src.files[0]);
        });
    }

    var src = document.getElementById("src");
    var target = document.getElementById("target");
    showImage(src,target);
</script>
