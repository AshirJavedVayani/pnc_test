<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update Employee
            <small>Fill your company information below </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0)">Employee</a></li>
            <li class="active">Update Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Employee</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{url('updateEmp')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Enter Employee First Name" value="{{$employee->firstname}}" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter Employee Last Name" value="{{$employee->lastname}}" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="company">Company</label>
                                <select class="form-control" name="company_id">
                                    <option value="{{$employee->company->id}}">{{$employee->company->name}}</option>
                                    @foreach($company as $comp)
                                    <option value="{{$comp->id}}">{{$comp->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address" value="{{$employee->email}}" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="number" name="phone" class="form-control" id="phone" placeholder="Enter Phone Number" value="{{$employee->phone}}" required>
                            </div>

                        </div>

                        <input type="hidden" name="empID" value="{{$employee->id}}">
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Update Employee</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <!--/.col (left) -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
