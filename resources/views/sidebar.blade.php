
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/public/img/logo-icon.png" class="img-circle" alt="Logo Image">
        </div>
        <div class="pull-left info">
          <p>{{ auth()->user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

<!--        <li class="active treeview">-->
<!--          <a href="#">-->
<!--            <i class="fa fa-dashboard"></i> <span>Dashboard</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu">-->
<!--            <li class="active"><a href="{{url('home')}}"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>-->
<!--            <li><a href="{{url('home')}}"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>-->
<!--          </ul>-->
<!--        </li>-->

	    <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>


        <li><a href="{{url('company')}}"><i class="fa fa-edit"></i> <span>Companies</span></a></li>
        <li><a href="{{url('employee')}}"><i class="fa fa-edit"></i> <span>Employees</span></a></li>



<!--        <li class="treeview">-->
<!--		  <a href="#">-->
<!--			<i class="fa fa-edit"></i> <span>Users</span>-->
<!--			<span class="pull-right-container">-->
<!--			  <i class="fa fa-angle-left pull-right"></i>-->
<!--			</span>-->
<!--		  </a>-->
<!--		  <ul class="treeview-menu">-->
<!--			  <li><a href="{{url('user')}}"><i class="fa fa-circle-o"></i> User </a></li>-->
<!--			  <li><a href="{{url('eback')}}"><i class="fa fa-circle-o"></i> Add User</a></li>-->
<!---->
<!--		  </ul>-->
<!--        </li>-->

<!--        <li class="treeview">-->
<!--          <a href="#">-->
<!--            <i class="fa fa-files-o"></i>-->
<!--            <span>Layout Options</span>-->
<!--            <span class="pull-right-container">-->
<!--              <span class="label label-primary pull-right">4</span>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu">-->
<!--            <li><a href="/public/pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>-->
<!--            <li><a href="/public/pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>-->
<!--            <li><a href="/public/pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>-->
<!--            <li><a href="/public/pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>-->
<!--          </ul>-->
<!--        </li>-->

<!--        <li class="treeview">-->
<!--          <a href="#">-->
<!--            <i class="fa fa-edit"></i> <span>Forms</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu">-->
<!--              <li><a href="{{url('upload')}}"><i class="fa fa-circle-o"></i> Upload CV</a></li>-->
<!--              <li><a href="{{url('eback')}}"><i class="fa fa-circle-o"></i> Education background</a></li>-->
<!--              <li><a href="{{url('grade')}}"><i class="fa fa-circle-o"></i> CSS Grade</a></li>-->
<!---->
<!--          </ul>-->
<!--        </li>-->


<!--        <li>-->
<!--          <a href="/public/pages/mailbox/mailbox.html">-->
<!--            <i class="fa fa-envelope"></i> <span>Mailbox</span>-->
<!--            <span class="pull-right-container">-->
<!--              <small class="label pull-right bg-yellow">12</small>-->
<!--              <small class="label pull-right bg-green">16</small>-->
<!--              <small class="label pull-right bg-red">5</small>-->
<!--            </span>-->
<!--          </a>-->
<!--        </li>-->

<!--        <li class="treeview">-->
<!--          <a href="#">-->
<!--            <i class="fa fa-share"></i> <span>Multilevel</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu">-->
<!--            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--            <li class="treeview">-->
<!--              <a href="#"><i class="fa fa-circle-o"></i> Level One-->
<!--                <span class="pull-right-container">-->
<!--                  <i class="fa fa-angle-left pull-right"></i>-->
<!--                </span>-->
<!--              </a>-->
<!--              <ul class="treeview-menu">-->
<!--                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>-->
<!--                <li class="treeview">-->
<!--                  <a href="#"><i class="fa fa-circle-o"></i> Level Two-->
<!--                    <span class="pull-right-container">-->
<!--                      <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </span>-->
<!--                  </a>-->
<!--                  <ul class="treeview-menu">-->
<!--                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                  </ul>-->
<!--                </li>-->
<!--              </ul>-->
<!--            </li>-->
<!--            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--          </ul>-->
<!--        </li>-->
<!--        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
<!--        <li class="header">LABELS</li>-->
<!--        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>-->
<!--        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>-->
<!--        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
