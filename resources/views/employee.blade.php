
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employee List
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0)">Employee</a></li>
            <li class="active">Employee List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Employee List</h3>
                        <a href="{{ url('addemployee') }}" type="button" class="btn btn-success" style="float: right;">Add</a>
                    </div>

                    @if(Session::has('flash_message'))
                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    @endif

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($emp as $employee)
                            <tr>

                                <td>{{++$start}}</td>
                                <td>{{$employee->firstname}}</td>
                                <td>{{$employee->lastname}}</td>
                                <td>{{$employee->company->name}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->phone}}</td>
                                <td>{{$employee->created_at}}</td>
                                <td>{{$employee->updated_at}}</td>

                                <td>
                                    <a href='{!! url("updateemployee/$employee->id") !!}' type="button" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                    <a href='javascript:void(0)' onclick="del('{{$employee->id}}')"type="button" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                                </td>

                            </tr>
                            @endforeach

                            </tbody>

                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

    function del(id){
        if (confirm("Are you sure you want to delete?")){
            window.location.href = '{!! url("deleteemployee/") !!}/'+id;
        }
    }

</script>
