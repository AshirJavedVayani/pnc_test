
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company List
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0)">Company</a></li>
            <li class="active">Company List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Company List</h3>
                        <a href="{{ url('addcompany') }}" type="button" class="btn btn-success" style="float: right;">Add</a>
                    </div>

                    @if(Session::has('flash_message'))
                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    @endif

                    @if($message = Session::get('success'))
                    {{ $message }}
                    @endif

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Website</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($comp as $company)
                            <tr>

                                <td>{{++$start}}</td>
                                <td>{{$company->name}}</td>
                                <td>{{$company->email}}</td>
                                <td><img src="{{$company->logo}}"></td>
                                <td>{{$company->website}}</td>
                                <td>{{$company->created_at}}</td>
                                <td>{{$company->updated_at}}</td>

                                <td>
                                    <a href='{!! url("updatecompany/$company->id") !!}' type="button" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                    <a href='javascript:void(0)' onclick="del('{{$company->id}}')"type="button" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                                </td>

                            </tr>
                            @endforeach

                            </tbody>

                        </table>

                        <div class="pagination">
                            {{ $comp->links() }}
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

    function del(id){
        if (confirm("Are you sure you want to delete?")){
            window.location.href = '{!! url("deletecompany/") !!}/'+id;
        }
    }

</script>
