<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Company
            <small>Fill your company information below </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0)">Company</a></li>
            <li class="active">Add Company</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Company</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    @endif

                    <form action="{{url('addComp')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Company Name" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="logo">Logo</label>
                                <input type="file" name="image" class="form-control" required>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Website</label>
                                <input type="text" name="website" class="form-control" id="website" placeholder="Enter Website Url" required>
                            </div>

                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Add Company</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->


            </div>
            <!--/.col (left) -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
