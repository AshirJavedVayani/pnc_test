<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//User Authentication
Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index');
Route::post('login', 'LoginController@authenticate')->name('login');
Route::get('logout', 'LoginController@logout');
Route::get('/home', 'DashboardController@index');

//Company
Route::get('/company', 'CompanyController@index');
Route::get('/addcompany', 'CompanyController@create');
Route::post('/addComp', 'CompanyController@store');
Route::get('/updatecompany/{CompID}', 'CompanyController@edit');
Route::post('/updateComp', 'CompanyController@update');
Route::get('/deletecompany/{CompID}', 'CompanyController@delete');


//Employee
Route::get('/employee', 'EmployeeController@index');
Route::get('/addemployee', 'EmployeeController@create');
Route::post('/addEmp', 'EmployeeController@store');
Route::get('/updateemployee/{EmpID}', 'EmployeeController@edit');
Route::post('/updateEmp', 'EmployeeController@update');
Route::get('/deleteemployee/{EmpID}', 'EmployeeController@delete');

