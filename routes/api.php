<?php

use Illuminate\Http\Request;
use App\Qualification;
use App\Http\Resources\Qualification as QualificationResource;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API Routes
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('employerRegister', 'API\EmployerController@employerRegister');
Route::post('forgetPassword', 'API\UserController@forgetPassword');
Route::post('qualification', 'API\QualificationController@get_qualifications');
Route::post('occupation', 'API\OccupationController@index');
Route::post('category', 'API\CategoryController@index');

Route::post('experience', 'API\ExperienceController@index');
Route::post('shift', 'API\ShiftController@index');
Route::post('career_level', 'API\CareerLevelController@index');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('user_details', 'API\UserController@details');
Route::post('updateUserDetails', 'API\UserController@updateUserDetails');
Route::post('upload_image', 'API\UserController@upload_image');
Route::post('logout', 'API\UserController@logout');
Route::post('change_password', 'API\UserController@change_password');
Route::get('career_tips', 'API\UserController@career_tips');
Route::get('get_cv', 'API\CVController@index');

//jobs
Route::get('jobs', 'API\JobController@index');
Route::post('applyForJob', 'API\JobController@applyForJob');
Route::get('appliedJobs', 'API\JobController@appliedJobs');
Route::get('featuredJobs', 'API\JobController@featuredJobs');

//notifications
Route::get('notifications', 'API\NotificationController@index');

//employers
Route::get('topEmployers', 'API\EmployerController@topEmployers');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
